function chuongTrinhChaohoi() {
  var thanhVien = document.getElementById("txt-thanhVien");
  var loiChao = thanhVien.options[thanhVien.selectedIndex].text;

  if (loiChao == "Bố") {
    document.getElementById("result").innerHTML = `Chào Bố Đã Đăng Nhập`;
  } else if (loiChao == "Mẹ") {
    document.getElementById("result").innerHTML = `Chào Mẹ Đã Đăng Nhập`;
  } else if (loiChao == "Anh Trai") {
    document.getElementById("result").innerHTML = `Chào Anh Đã Đăng Nhập`;
  } else if (loiChao == "Em Gái") {
    document.getElementById("result").innerHTML = `Chào Em Đã Đăng Nhập`;
  } else {
    document.getElementById("result").innerHTML = `Chào người lạ ơi!!`;
  }
}
